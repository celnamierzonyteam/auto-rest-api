package database

import (
	"auto-rest-api/internal/config"
	"fmt"
)

const PostgresDatabaseEngine = "postgres"

type Database interface {
	GetTables() ([]string, error)
	GetAllRows(tableName string) ([]map[string]interface{}, error)
	GetRow(tableName string, id string) (map[string]interface{}, error)
	Insert(tableName string, row map[string]interface{}) error
	Delete(tableName string, id string) error
	Update(tableName string, id string, row map[string]interface{}) error
}

func NewDatabase(config *config.DatabaseConfig) (Database, error) {
	switch config.Engine {
	case "postgres":
		return NewPostgresDatabase(config)
	default:
		return nil, fmt.Errorf("usupported database engine")
	}
}
