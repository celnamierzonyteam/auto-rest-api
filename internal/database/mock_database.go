package database

type MockDatabase struct {
	GetTablesMock  func() ([]string, error)
	GetAllRowsMock func(tableName string) ([]map[string]interface{}, error)
	GetRowMock     func(tableName string, id string) (map[string]interface{}, error)
	InsertMock     func(tableName string, row map[string]interface{}) error
	DeleteMock     func(tableName string, id string) error
	UpdateMock     func(tableName string, id string, row map[string]interface{}) error
}

func (m MockDatabase) GetTables() ([]string, error) {
	return m.GetTablesMock()
}

func (m MockDatabase) GetAllRows(tableName string) ([]map[string]interface{}, error) {
	return m.GetAllRowsMock(tableName)
}

func (m MockDatabase) GetRow(tableName string, id string) (map[string]interface{}, error) {
	return m.GetRowMock(tableName, id)
}

func (m MockDatabase) Insert(tableName string, row map[string]interface{}) error {
	return m.InsertMock(tableName, row)
}

func (m MockDatabase) Delete(tableName string, id string) error {
	return m.DeleteMock(tableName, id)
}

func (m MockDatabase) Update(tableName string, id string, row map[string]interface{}) error {
	return m.UpdateMock(tableName, id, row)
}
