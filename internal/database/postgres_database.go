package database

import (
	"auto-rest-api/internal/config"
	"auto-rest-api/internal/service/errorhandlers"
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"strconv"
	"strings"
)

type PostgresDatabase struct {
	connectionString string
	db               *pgx.Conn
}

func NewPostgresDatabase(config *config.DatabaseConfig) (*PostgresDatabase, error) {
	connectionString := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s",
		config.Username, config.Password, config.Hostname, config.Port, config.DbName)

	postgresDb := &PostgresDatabase{
		connectionString: connectionString,
		db:               nil,
	}

	return postgresDb, nil
}

func (pg *PostgresDatabase) GetTables() ([]string, error) {
	err := pg.connect()
	if err != nil {
		return nil, err
	}
	defer pg.disconnect()

	var result []string

	rows, err := pg.db.Query(context.Background(), "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';")
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var tableName string
		err = rows.Scan(&tableName)
		if err != nil {
			return nil, err
		}

		result = append(result, tableName)
	}
	return result, nil
}

func (pg *PostgresDatabase) GetAllRows(tableName string) ([]map[string]interface{}, error) {
	err := pg.connect()
	if err != nil {
		return nil, err
	}
	defer pg.disconnect()

	var result []map[string]interface{}

	rows, err := pg.db.Query(context.Background(), fmt.Sprintf("SELECT * FROM %s;", tableName))
	if err != nil {
		return nil, err
	}

	fieldsDesc := rows.FieldDescriptions()

	for rows.Next() {
		interfaces, err := rows.Values()
		if err != nil {
			return nil, err
		}

		mappedRow := map[string]interface{}{}
		for i, cell := range interfaces {
			mappedRow[string(fieldsDesc[i].Name)] = cell
		}

		result = append(result, mappedRow)
	}

	return result, nil
}

func (pg *PostgresDatabase) Insert(tableName string, row map[string]interface{}) error {
	err := pg.connect()
	if err != nil {
		return err
	}
	defer pg.disconnect()

	var fieldNames []string
	var fieldValues []interface{}
	var parameterCounter []string

	i := 1
	for columnName, value := range row {
		fieldNames = append(fieldNames, columnName)
		fieldValues = append(fieldValues, value)

		parameterCounter = append(parameterCounter, "$"+strconv.Itoa(i))

		i++
	}

	fieldString := strings.Join(fieldNames, ", ")
	parameterCounterString := strings.Join(parameterCounter, ", ")

	_, err = pg.db.Exec(context.Background(), fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s);", tableName, fieldString, parameterCounterString), fieldValues...)
	if err != nil {
		return err
	}

	return nil
}

func (pg *PostgresDatabase) GetRow(tableName string, id string) (map[string]interface{}, error) {
	err := pg.connect()
	if err != nil {
		return nil, err
	}
	defer pg.disconnect()

	primaryKeyName, fieldType, err := pg.getPrimaryKey(tableName)
	if err != nil {
		return nil, err
	}

	result := make(map[string]interface{})

	convertedId, err := convertStringToType(id, fieldType)
	if err != nil {
		return nil, err
	}

	rows, err := pg.db.Query(context.Background(), fmt.Sprintf("SELECT * FROM %s WHERE %s = $1", tableName, primaryKeyName), convertedId)
	if err != nil {
		return nil, err
	}

	fieldsDesc := rows.FieldDescriptions()

	if rows.Next() {
		interfaces, err := rows.Values()
		if err != nil {
			return nil, err
		}

		for i, cell := range interfaces {
			result[string(fieldsDesc[i].Name)] = cell
		}
	} else {
		return nil, errorhandlers.NewRecordNotFoundError(tableName, primaryKeyName, id)
	}

	return result, nil
}

func (pg *PostgresDatabase) Delete(tableName string, id string) error {
	err := pg.connect()
	if err != nil {
		return err
	}
	defer pg.disconnect()

	primaryKeyName, fieldType, err := pg.getPrimaryKey(tableName)
	if err != nil {
		return err
	}

	convertedId, err := convertStringToType(id, fieldType)
	if err != nil {
		return err
	}

	commandTag, err := pg.db.Exec(context.Background(), fmt.Sprintf("DELETE FROM %s WHERE %s = $1", tableName, primaryKeyName), convertedId)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() < 1 {
		return errorhandlers.NewRecordNotFoundError(tableName, primaryKeyName, id)
	}

	return nil
}

func (pg *PostgresDatabase) Update(tableName string, id string, row map[string]interface{}) error {
	err := pg.connect()
	if err != nil {
		return err
	}
	defer pg.disconnect()

	primaryKeyName, fieldType, err := pg.getPrimaryKey(tableName)
	if err != nil {
		return err
	}

	convertedId, err := convertStringToType(id, fieldType)
	if err != nil {
		return err
	}

	i := 2
	var fieldValues []interface{}
	fieldValues = append(fieldValues, convertedId)

	var fieldSets []string
	for columnName, value := range row {
		fieldValues = append(fieldValues, value)
		fieldSets = append(fieldSets, columnName+" = $"+strconv.Itoa(i))
		i++
	}

	fieldString := strings.Join(fieldSets, ". ")

	commandTag, err := pg.db.Exec(context.Background(),
		fmt.Sprintf("UPDATE %s SET %s WHERE %s = $1;", tableName, fieldString, primaryKeyName),
		fieldValues...)
	if err != nil {
		return err
	}

	if commandTag.RowsAffected() < 1 {
		return errorhandlers.NewRecordNotFoundError(tableName, primaryKeyName, id)
	}

	return nil
}

func (pg *PostgresDatabase) getPrimaryKey(tableName string) (string, string, error) {
	queryF := `SELECT c.column_name, c.data_type
	FROM information_schema.table_constraints tc
	JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name)
	JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema
	AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
	WHERE constraint_type = 'PRIMARY KEY' and tc.table_name = $1`

	var primaryKeyName string
	var fieldType string
	row := pg.db.QueryRow(context.Background(), queryF, tableName)
	err := row.Scan(&primaryKeyName, &fieldType)

	return primaryKeyName, fieldType, err
}

func convertStringToType(value string, fieldType string) (interface{}, error) {
	if strings.Contains(fieldType, "int") {
		result, err := strconv.Atoi(value)
		if err != nil {
			return nil, err
		}
		return result, nil
	}

	return value, nil
}

func (pd *PostgresDatabase) connect() error {
	var err error
	pd.db, err = pgx.Connect(context.Background(), pd.connectionString)
	if err != nil {
		return err
	}

	err = pd.db.Ping(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func (pd *PostgresDatabase) disconnect() {
	err := pd.db.Close(context.Background())
	if err != nil {
		fmt.Printf("database disconnect error: %s", err.Error())
	}
}
