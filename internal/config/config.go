package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type Config struct {
	Database *DatabaseConfig `yaml:"database"`
	Server   *ServerConfig   `yaml:"server"`
	Security *SecurityConfig `yaml:"security"`
}

type DatabaseConfig struct {
	Engine   string `yaml:"engine"`
	Hostname string `yaml:"hostname"`
	Port     string `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	DbName   string `yaml:"db_name"`
}

type ServerConfig struct {
	Port      string `yaml:"port"`
	UrlPrefix string `yaml:"url_prefix"`
}

type SecurityConfig struct {
	TrustedProxies []string `yaml:"trusted_proxies"`
}

func LoadConfig(configFile string) (*Config, error) {
	// Checking configuration file
	if _, err := os.Stat(configFile); err != nil {
		return nil, fmt.Errorf("cannot find configuration file")
	}

	//Loading configuration
	var cfg Config
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(yamlFile, &cfg); err != nil {
		return nil, err
	}

	//Validation configuration
	if err = cfg.validate(); err != nil {
		return nil, err
	}

	return &cfg, nil
}

func (cfg *Config) validate() error {
	if cfg.Database.Engine == "" {
		return fmt.Errorf("validation error: missing field database.engine")
	}

	if cfg.Database.Hostname == "" {
		return fmt.Errorf("validation error: missing field database.hostname")
	}

	if cfg.Database.Port == "" {
		return fmt.Errorf("validation error: missing field database.port")
	}

	if cfg.Database.Username == "" {
		return fmt.Errorf("validation error: missing field database.username")
	}

	if cfg.Database.Password == "" {
		return fmt.Errorf("validation error: missing field database.password")
	}

	if cfg.Database.DbName == "" {
		return fmt.Errorf("validation error: missing field database.db_name")
	}

	if cfg.Server.Port == "" {
		return fmt.Errorf("validation error: missing field server.password")
	}

	if cfg.Database.Engine != "postgres" {
		return fmt.Errorf("unsupported database engine")
	}

	return nil
}
