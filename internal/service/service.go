package service

import (
	"auto-rest-api/internal/config"
	"auto-rest-api/internal/database"
	"github.com/gin-gonic/gin"
)

type Service struct {
	config   *config.Config
	database database.Database

	Tables []string
}

func NewRestService(config *config.Config, database database.Database) *Service {
	return &Service{config: config, database: database}
}

func (s *Service) Run() error {
	router := gin.New()

	var err error

	if len(s.config.Security.TrustedProxies) > 0 {
		err := router.SetTrustedProxies(s.config.Security.TrustedProxies)
		if err != nil {
			return err
		}
	}

	s.Tables, err = s.database.GetTables()
	if err != nil {
		return err
	}

	InitRouter(router, s)

	err = router.Run(":" + s.config.Server.Port)
	return err
}
