package service

import (
	"auto-rest-api/internal/config"
	"auto-rest-api/internal/database"
	"auto-rest-api/internal/service/errorhandlers"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestNewHandlers(t *testing.T) {
	handlers := NewHandlers(nil)
	assert.NotNil(t, handlers)
}

func TestHandlers_GetTablesHandler(t *testing.T) {
	testCases := []struct {
		Name         string
		Tables       []string
		ExpectedJson string
	}{
		{
			Name:         "NoTables",
			Tables:       []string{},
			ExpectedJson: "{\"tables\":[]}",
		},
		{
			Name:         "WithTables",
			Tables:       []string{"table_1", "table_2"},
			ExpectedJson: "{\"tables\":[\"table_1\",\"table_2\"]}",
		},
	}

	gin.SetMode(gin.TestMode)
	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			service, err := createMockService()
			assert.NoError(t, err)

			service.Tables = tc.Tables

			handlers := NewHandlers(service)

			w := httptest.NewRecorder()
			ginContext, _ := gin.CreateTestContext(w)
			handlers.GetTablesHandler(ginContext)

			assert.Equal(t, http.StatusOK, w.Code)

			assert.NoError(t, err)
			assert.Equal(t, tc.ExpectedJson, w.Body.String())
		})
	}
}

func TestHandlers_GetTableRowsHandler(t *testing.T) {
	testCases := []struct {
		Name             string
		TableName        string
		Tables           []string
		ExpectedHttpCode int
		ExpectedJson     string
		DatabaseError    error
		Rows             []map[string]interface{}
	}{
		{
			Name:             "WithNoExistingTable",
			TableName:        "no_existing_table",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Resource not found\"}",
		},
		{
			Name:             "WithDatabaseError",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusInternalServerError,
			ExpectedJson:     "{\"error\":500,\"message\":\"generic database error\"}",
			DatabaseError:    fmt.Errorf("generic database error"),
		},
		{
			Name:             "WithEmptyResult",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusOK,
			ExpectedJson:     "[]",
			Rows:             []map[string]interface{}{},
		},
		{
			Name:             "WithRowsInResult",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusOK,
			ExpectedJson:     "[{\"AAA\":\"BBB\"},{\"AAA\":\"CCC\"}]",
			Rows: []map[string]interface{}{
				{
					"AAA": "BBB",
				},
				{
					"AAA": "CCC",
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			cfg := &config.Config{
				Database: &config.DatabaseConfig{
					Engine: database.PostgresDatabaseEngine,
				},
				Server:   &config.ServerConfig{},
				Security: &config.SecurityConfig{},
			}

			db := database.MockDatabase{
				GetAllRowsMock: func(tableName string) ([]map[string]interface{}, error) {
					return tc.Rows, tc.DatabaseError
				},
			}

			service := NewRestService(cfg, db)
			service.Tables = tc.Tables

			handlers := NewHandlers(service)

			w := httptest.NewRecorder()
			ginContext, _ := gin.CreateTestContext(w)
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "table",
				Value: tc.TableName,
			})
			handlers.GetTableRowsHandler(ginContext)

			assert.Equal(t, tc.ExpectedHttpCode, w.Code)
			assert.Equal(t, tc.ExpectedJson, w.Body.String())
		})
	}
}

func TestHandlers_GetTableRowHandler(t *testing.T) {
	testCases := []struct {
		Name             string
		TableName        string
		Tables           []string
		Row              map[string]interface{}
		DatabaseError    error
		ExpectedHttpCode int
		ExpectedJson     string
	}{
		{
			Name:             "WithNoExistingTable",
			TableName:        "no_existing_table_name",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Resource not found\"}",
		},
		{
			Name:             "WithDatabaseError",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusInternalServerError,
			ExpectedJson:     "{\"error\":500,\"message\":\"generic database error\"}",
			DatabaseError:    fmt.Errorf("generic database error"),
		},
		{
			Name:             "WhenRowDoesNotExists",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Record not found in table table_1, where id = 2\"}",
			DatabaseError:    errorhandlers.NewRecordNotFoundError("table_1", "id", "2"),
		},
		{
			Name:             "WithRowInResult",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusOK,
			ExpectedJson:     "{\"AAA\":1,\"BBB\":\"CCC\"}",
			Row: map[string]interface{}{
				"AAA": 1,
				"BBB": "CCC",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			cfg := &config.Config{
				Database: &config.DatabaseConfig{
					Engine: database.PostgresDatabaseEngine,
				},
				Server:   &config.ServerConfig{},
				Security: &config.SecurityConfig{},
			}

			db := database.MockDatabase{
				GetRowMock: func(tableName string, id string) (map[string]interface{}, error) {
					return tc.Row, tc.DatabaseError
				},
			}

			service := NewRestService(cfg, db)
			service.Tables = tc.Tables

			w := httptest.NewRecorder()
			ginContext, _ := gin.CreateTestContext(w)
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "table",
				Value: tc.TableName,
			})
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "id",
				Value: "2",
			})
			handlers := NewHandlers(service)

			handlers.GetTableRowHandler(ginContext)

			assert.Equal(t, tc.ExpectedHttpCode, w.Code)
			assert.Equal(t, tc.ExpectedJson, w.Body.String())
		})
	}
}

func TestHandlers_InsertTableRowHandler(t *testing.T) {
	testCases := []struct {
		Name             string
		TableName        string
		Tables           []string
		DatabaseError    error
		ExpectedHttpCode int
		ExpectedJson     string
		RequestBody      string
	}{
		{
			Name:             "WithNoExistingTable",
			TableName:        "no_existing_table_name",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Resource not found\"}",
		},
		{
			Name:             "WithIncorrectJsonRequest",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusBadRequest,
			ExpectedJson:     "{\"error\":400,\"message\":\"unexpected EOF\"}",
			RequestBody:      "[[[[[",
		},
		{
			Name:             "WithDatabaseError",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			DatabaseError:    fmt.Errorf("generic database error"),
			ExpectedHttpCode: http.StatusInternalServerError,
			ExpectedJson:     "{\"error\":500,\"message\":\"generic database error\"}",
			RequestBody:      "{}",
		},
		{
			Name:             "WithEverythingCorrect",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusCreated,
			ExpectedJson:     "{\"aaa\":\"bbb\"}",
			RequestBody:      "{\"aaa\":\"bbb\"}",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			cfg := &config.Config{
				Database: &config.DatabaseConfig{
					Engine: database.PostgresDatabaseEngine,
				},
				Server:   &config.ServerConfig{},
				Security: &config.SecurityConfig{},
			}

			db := database.MockDatabase{
				InsertMock: func(tableName string, row map[string]interface{}) error {
					return tc.DatabaseError
				},
			}

			service := NewRestService(cfg, db)
			service.Tables = tc.Tables

			w := httptest.NewRecorder()
			ginContext, _ := gin.CreateTestContext(w)
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "table",
				Value: tc.TableName,
			})

			requestReader := io.NopCloser(strings.NewReader(tc.RequestBody))
			httpRequest := httptest.NewRequest("POST", "/", requestReader)
			ginContext.Request = httpRequest

			handlers := NewHandlers(service)

			handlers.InsertTableRowHandler(ginContext)

			assert.Equal(t, tc.ExpectedHttpCode, w.Code)
			assert.Equal(t, tc.ExpectedJson, w.Body.String())
		})
	}
}

func TestHandlers_DeleteTableRowHandler(t *testing.T) {
	testCases := []struct {
		Name             string
		TableName        string
		Tables           []string
		DatabaseError    error
		ExpectedHttpCode int
		ExpectedJson     string
	}{
		{
			Name:             "WithNoExistingTable",
			TableName:        "no_existing_table",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Resource not found\"}",
		},
		{
			Name:             "WithNotExistingRecord",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			DatabaseError:    errorhandlers.NewRecordNotFoundError("table_1", "id", "2"),
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Record not found in table table_1, where id = 2\"}",
		},
		{
			Name:             "WithDatabaseError",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			DatabaseError:    fmt.Errorf("generic database error"),
			ExpectedHttpCode: http.StatusInternalServerError,
			ExpectedJson:     "{\"error\":500,\"message\":\"generic database error\"}",
		},
		{
			Name:             "WithDeletedRecord",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNoContent,
			ExpectedJson:     "",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			cfg := &config.Config{
				Database: &config.DatabaseConfig{
					Engine: database.PostgresDatabaseEngine,
				},
				Server:   &config.ServerConfig{},
				Security: &config.SecurityConfig{},
			}

			db := database.MockDatabase{
				DeleteMock: func(tableName string, id string) error {
					return tc.DatabaseError
				},
			}

			service := NewRestService(cfg, db)
			service.Tables = tc.Tables

			w := httptest.NewRecorder()
			ginContext, _ := gin.CreateTestContext(w)
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "table",
				Value: tc.TableName,
			})
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "id",
				Value: "2",
			})

			handlers := NewHandlers(service)
			handlers.DeleteTableRowHandler(ginContext)

			assert.Equal(t, tc.ExpectedHttpCode, w.Code)
			assert.Equal(t, tc.ExpectedJson, w.Body.String())
		})
	}
}

func TestHandlers_UpdateTableRowHandler(t *testing.T) {
	testCases := []struct {
		Name             string
		TableName        string
		Tables           []string
		DatabaseError    error
		RequestBody      string
		ExpectedHttpCode int
		ExpectedJson     string
	}{
		{
			Name:             "WithNoExistingTable",
			TableName:        "no_existing_table",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Resource not found\"}",
		},
		{
			Name:             "WithEmptyRequest",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			ExpectedHttpCode: http.StatusBadRequest,
			ExpectedJson:     "{\"error\":400,\"message\":\"EOF\"}",
		},
		{
			Name:             "WithNotExistingRecord",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			RequestBody:      "{\"aaa\":\"bbb\"}",
			DatabaseError:    errorhandlers.NewRecordNotFoundError("table_1", "id", "2"),
			ExpectedHttpCode: http.StatusNotFound,
			ExpectedJson:     "{\"error\":404,\"message\":\"Record not found in table table_1, where id = 2\"}",
		},
		{
			Name:             "WithDatabaseError",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			RequestBody:      "{\"aaa\":\"bbb\"}",
			DatabaseError:    fmt.Errorf("generic database error"),
			ExpectedHttpCode: http.StatusInternalServerError,
			ExpectedJson:     "{\"error\":500,\"message\":\"generic database error\"}",
		},
		{
			Name:             "WithUpdatedRecord",
			TableName:        "table_1",
			Tables:           []string{"table_1", "table_2"},
			RequestBody:      "{\"aaa\":\"bbb\"}",
			ExpectedHttpCode: http.StatusNoContent,
			ExpectedJson:     "",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			cfg := &config.Config{
				Database: &config.DatabaseConfig{
					Engine: database.PostgresDatabaseEngine,
				},
				Server:   &config.ServerConfig{},
				Security: &config.SecurityConfig{},
			}

			db := database.MockDatabase{
				UpdateMock: func(tableName string, id string, row map[string]interface{}) error {
					return tc.DatabaseError
				},
			}

			service := NewRestService(cfg, db)
			service.Tables = tc.Tables

			w := httptest.NewRecorder()
			ginContext, _ := gin.CreateTestContext(w)
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "table",
				Value: tc.TableName,
			})
			ginContext.Params = append(ginContext.Params, gin.Param{
				Key:   "id",
				Value: "2",
			})

			requestReader := io.NopCloser(strings.NewReader(tc.RequestBody))
			httpRequest := httptest.NewRequest("POST", "/", requestReader)
			ginContext.Request = httpRequest

			handlers := NewHandlers(service)
			handlers.UpdateTableRowHandler(ginContext)

			assert.Equal(t, tc.ExpectedHttpCode, w.Code)
			assert.Equal(t, tc.ExpectedJson, w.Body.String())
		})
	}
}

func createMockService() (*Service, error) {
	cfg := &config.Config{
		Database: &config.DatabaseConfig{
			Engine: database.PostgresDatabaseEngine,
		},
		Server:   &config.ServerConfig{},
		Security: &config.SecurityConfig{},
	}
	db, err := database.NewDatabase(cfg.Database)
	if err != nil {
		return nil, err
	}

	service := NewRestService(cfg, db)
	return service, nil
}
