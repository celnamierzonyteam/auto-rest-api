package service

import (
	"auto-rest-api/internal/service/errorhandlers"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Handlers struct {
	service *Service
}

func NewHandlers(s *Service) *Handlers {
	return &Handlers{s}
}

func (h Handlers) GetTablesHandler(g *gin.Context) {
	g.JSON(http.StatusOK, gin.H{
		"tables": h.service.Tables,
	})
}

func (h Handlers) GetTableRowsHandler(g *gin.Context) {
	tableName := g.Param("table")
	if !isTableExists(h.service.Tables, tableName) {
		g.JSON(http.StatusNotFound, gin.H{
			"error":   404,
			"message": "Resource not found",
		})
		return
	}

	rows, err := h.service.database.GetAllRows(tableName)
	if err != nil {
		g.JSON(http.StatusInternalServerError, gin.H{
			"error":   500,
			"message": err.Error(),
		})
		return
	}

	g.JSON(http.StatusOK, rows)
}

func (h Handlers) GetTableRowHandler(g *gin.Context) {
	tableName := g.Param("table")
	if !isTableExists(h.service.Tables, tableName) {
		g.JSON(http.StatusNotFound, gin.H{
			"error":   404,
			"message": "Resource not found",
		})
		return
	}

	id := g.Param("id")

	result, err := h.service.database.GetRow(tableName, id)
	if err != nil {
		if _, ok := err.(errorhandlers.RecordNotFoundError); ok {
			g.JSON(http.StatusNotFound, gin.H{
				"error":   404,
				"message": err.Error(),
			})
		} else {
			g.JSON(http.StatusInternalServerError, gin.H{
				"error":   500,
				"message": err.Error(),
			})
		}
		return
	}

	g.JSON(http.StatusOK, result)
}

func (h Handlers) InsertTableRowHandler(g *gin.Context) {
	tableName := g.Param("table")
	if !isTableExists(h.service.Tables, tableName) {
		g.JSON(http.StatusNotFound, gin.H{
			"error":   404,
			"message": "Resource not found",
		})
		return
	}

	var request map[string]interface{}
	err := g.BindJSON(&request)
	if err != nil {
		g.JSON(http.StatusBadRequest, gin.H{
			"error":   400,
			"message": err.Error(),
		})
		return
	}

	err = h.service.database.Insert(tableName, request)
	if err != nil {
		g.JSON(http.StatusInternalServerError, gin.H{
			"error":   500,
			"message": err.Error(),
		})
		return
	}

	g.JSON(http.StatusCreated, request)
}

func (h Handlers) DeleteTableRowHandler(g *gin.Context) {
	tableName := g.Param("table")
	if !isTableExists(h.service.Tables, tableName) {
		g.JSON(http.StatusNotFound, gin.H{
			"error":   404,
			"message": "Resource not found",
		})
		return
	}

	id := g.Param("id")
	err := h.service.database.Delete(tableName, id)
	if err != nil {
		if _, ok := err.(errorhandlers.RecordNotFoundError); ok {
			g.JSON(http.StatusNotFound, gin.H{
				"error":   404,
				"message": err.Error(),
			})
		} else {
			g.JSON(http.StatusInternalServerError, gin.H{
				"error":   500,
				"message": err.Error(),
			})
		}
		return
	}

	g.String(http.StatusNoContent, "")
}

func (h Handlers) UpdateTableRowHandler(g *gin.Context) {
	tableName := g.Param("table")
	if !isTableExists(h.service.Tables, tableName) {
		g.JSON(http.StatusNotFound, gin.H{
			"error":   404,
			"message": "Resource not found",
		})
		return
	}

	var request map[string]interface{}
	err := g.BindJSON(&request)
	if err != nil {
		g.JSON(http.StatusBadRequest, gin.H{
			"error":   400,
			"message": err.Error(),
		})
		return
	}

	id := g.Param("id")
	err = h.service.database.Update(tableName, id, request)
	if err != nil {
		if _, ok := err.(errorhandlers.RecordNotFoundError); ok {
			g.JSON(http.StatusNotFound, gin.H{
				"error":   404,
				"message": err.Error(),
			})
		} else {
			g.JSON(http.StatusInternalServerError, gin.H{
				"error":   500,
				"message": err.Error(),
			})
		}
		return
	}

	g.String(http.StatusNoContent, "")
}

func isTableExists(tables []string, table string) bool {
	for _, t := range tables {
		if t == table {
			return true
		}
	}

	return false
}
