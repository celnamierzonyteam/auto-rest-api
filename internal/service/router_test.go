package service

import (
	"auto-rest-api/internal/config"
	"auto-rest-api/internal/database"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInitRouter(t *testing.T) {
	cfg := &config.Config{
		Database: &config.DatabaseConfig{
			Engine: database.PostgresDatabaseEngine,
		},
		Server:   &config.ServerConfig{},
		Security: &config.SecurityConfig{},
	}
	db, err := database.NewDatabase(cfg.Database)
	assert.NoError(t, err)

	router := gin.New()
	service := NewRestService(cfg, db)

	InitRouter(router, service)
	routes := router.Routes()
	assert.Len(t, routes, 6)
}
