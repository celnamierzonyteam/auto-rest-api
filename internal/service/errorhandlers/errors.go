package errorhandlers

import "fmt"

type RecordNotFoundError struct {
	tableName  string
	fieldName  string
	fieldValue string
}

func (e RecordNotFoundError) Error() string {
	return fmt.Sprintf("Record not found in table %s, where %s = %s", e.tableName, e.fieldName, e.fieldValue)
}

func NewRecordNotFoundError(tableName string, fieldName string, fieldValue string) RecordNotFoundError {
	return RecordNotFoundError{
		tableName:  tableName,
		fieldName:  fieldName,
		fieldValue: fieldValue,
	}
}
