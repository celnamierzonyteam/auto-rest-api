package service

import (
	"auto-rest-api/internal/config"
	"auto-rest-api/internal/database"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewRestService(t *testing.T) {
	cfg := &config.Config{
		Database: &config.DatabaseConfig{
			Engine: database.PostgresDatabaseEngine,
		},
		Server:   &config.ServerConfig{},
		Security: &config.SecurityConfig{},
	}
	db, err := database.NewDatabase(cfg.Database)
	assert.NoError(t, err)

	service := NewRestService(cfg, db)
	assert.NotNil(t, service)
}
