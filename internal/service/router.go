package service

import (
	"github.com/gin-gonic/gin"
)

func InitRouter(router *gin.Engine, s *Service) {
	handlers := NewHandlers(s)

	router.GET(s.config.Server.UrlPrefix, handlers.GetTablesHandler)
	router.GET(s.config.Server.UrlPrefix+"/:table", handlers.GetTableRowsHandler)
	router.GET(s.config.Server.UrlPrefix+"/:table/:id", handlers.GetTableRowHandler)
	router.POST(s.config.Server.UrlPrefix+"/:table", handlers.InsertTableRowHandler)
	router.DELETE(s.config.Server.UrlPrefix+"/:table/:id", handlers.DeleteTableRowHandler)
	router.PATCH(s.config.Server.UrlPrefix+"/:table/:id", handlers.UpdateTableRowHandler)
}
