module auto-rest-api

go 1.15

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/jackc/pgx/v4 v4.14.1
	github.com/lib/pq v1.10.4 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
