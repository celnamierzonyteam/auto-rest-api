package main

import (
	"auto-rest-api/internal/config"
	"auto-rest-api/internal/database"
	"auto-rest-api/internal/service"
	"log"
	"os"
)

func main() {
	cfg, err := config.LoadConfig("config.yaml")
	if err != nil {
		log.Printf("configuration error: %s\n", err.Error())
		os.Exit(1)
	}

	db, err := database.NewDatabase(cfg.Database)
	if err != nil {
		log.Printf("database connection error: %s\n", err.Error())
	}

	srv := service.NewRestService(cfg, db)
	if err = srv.Run(); err != nil {
		log.Fatalf("server error: %s", err.Error())
	}
}
